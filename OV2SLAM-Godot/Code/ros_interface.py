from godot import exposed, export
from godot import *


import rospy
from std_msgs.msg import String
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Odometry
import threading
import time

""" 
Python-Godot doesn't seem to able to update objects translation/rotation,
so the best solution is to read and save ROS data here
"""


@exposed
class ros_slam(Node):
	def callback(self, data):
		# Read translation and rotation camera pose info from ROS topic
		self.t_x = data.pose.position.x
		self.t_y = data.pose.position.y
		self.t_z = data.pose.position.z
		
		self.r_x = data.pose.orientation.x
		self.r_y = data.pose.orientation.y
		self.r_z = data.pose.orientation.z
		self.r_w = data.pose.orientation.w
		print(self.t_x)


	def listen(self):
		rospy.init_node('listener', anonymous=True)
		rospy.Subscriber("/ov2slam_node/vo_pose", PoseStamped, self.callback)
		rospy.spin()


	def _ready(self):
		self.r_x = 0
		self.r_y = 0
		self.r_z = 0
		self.r_w = 0

		self.t_x = 0
		self.t_y = 0
		self.t_z = 0

		self.t = threading.Thread(target=self.listen)
		rospy.init_node('listener', anonymous=True)
		# Start separate non blocking thread to read ROS camera pose data
		self.t.start()
