extends Node


var pyros = null
var translation_scale = 4

""" 
This reads ROS camera pose data from the Python thread and updates the camera
"""

func _ready():
	pyros = get_node("../PyRos")


func _process(delta):
	# Convert quartenion to euler and flip some axes
	var q = Quat(pyros.r_x, pyros.r_y, pyros.r_z, pyros.r_w)
	var e = q.get_euler()
	e.y *= -1
	e.z *=-1
	self.rotation = e

	self.transform.origin.x = pyros.t_x*translation_scale
	self.transform.origin.y = -pyros.t_y*translation_scale
	self.transform.origin.z = -pyros.t_z*translation_scale
