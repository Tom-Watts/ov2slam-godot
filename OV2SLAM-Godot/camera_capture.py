import rospy
from sensor_msgs.msg import Image
import time

import cv2
from cv_bridge import CvBridge, CvBridgeError
import threading
import numpy as np

"""
These settings are being run with two OV2710
You will need adjust frame rate and capture size according to your camera
"""


rospy.init_node('VideoPublisher', anonymous=True)

cam1 = rospy.Publisher('/cam0/image_raw', Image, queue_size=1)
cam2 = rospy.Publisher('/cam1/image_raw', Image, queue_size=1)


class Cap(object):
    def __init__(self, cap):
        self.cap = cap
        fourcc = cv2.VideoWriter_fourcc(*'MJPG')
        self.cap.set(cv2.CAP_PROP_FOURCC, fourcc)
        self.cap.set(cv2.CAP_PROP_FPS, 120)
        self.cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
        self.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
        self.img = None

    def capture(self):
        _, self.img = self.cap.read()
        if self.img is None: return
        self.img = cv2.flip(self.img, -1)
        self.img = cv2.cvtColor(self.img, cv2.COLOR_BGR2GRAY)


bridge = CvBridge()

cap1 = Cap(cv2.VideoCapture(0))
cap2 = Cap(cv2.VideoCapture(2))


while True:
    t1 = threading.Thread(target=cap1.capture)
    t2 = threading.Thread(target=cap2.capture)
    t1.start()
    t2.start()
    t1.join()
    t2.join()

    if cap1.img is not None:
        msg_frame1 = bridge.cv2_to_imgmsg(cap1.img, "mono8")
        msg_frame2 = bridge.cv2_to_imgmsg(cap2.img, "mono8")
        cam1.publish(msg_frame1)
        cam2.publish(msg_frame2)
        cv2.imshow('img', np.hstack((cap1.img, cap2.img)))

        key = cv2.waitKey(1)
        if key == 27:
            break
