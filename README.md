Demo running OV2SLAM (https://github.com/ov2slam/ov2slam) in real-time using Godot 3.2.3 <br>

To run, in separate terminals type: <br>
roscore <br>
source your_catkin_workspace/devel/setup.bash & rosrun ov2slam ov2slam_node your_settings.yaml <br>
python3 camera_capture.py <br><br>

Then run it in Godot! <br>

This is using Python-Godot (https://github.com/touilleMan/godot-python) with only the x11 module installed.
You will need to set your Python library paths in the Godot editor, under settings, Python Script, Path. <br>

For questions/issues email: zonkosoft@gmail.com =)
